<?php

return [
    'name'        => 'Undefined Module',
    'author'      => 'Undefined Author',
    'description' => 'Undefined Description',
    'namespace'   => 'Skimia\\Portfolio',
    'require'     => ['skimia.pages']
];