<?php

$app = Angular::get(OS_APPLICATION_NAME);


$app->addState('jobs_manager','/jobs-manager',function($params){
    return View::make('skimia.pages::activities.pages-manager.index',$params);
});

\Skimia\Portfolio\Data\Forms\JobsCRUDForm::register($app,'jobs_manager');
\Skimia\Portfolio\Data\Forms\JobTypesCRUDForm::register($app,'jobs_manager');
