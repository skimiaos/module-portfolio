<?php

return [


    'jobs' => [
        'list'=>[
            'type'    =>['label'=>'Type de client'],
            'picture'    =>['label'=>'Logo'],
            'client'    =>['label'=>'Entreprise du client'],
        ],
        'fields'=>[
            'type'    =>['label'=>'Type de client'],
            'client'    =>['label'=>'Nom du client'],
            'caption'    =>['label'=>'Déscription'],
            'testimonials'    =>['label'=>'Témoignage'],


            'picture'             =>['label'=>'Logo client'],
        ],
        'picture'             =>[
            'fields'=>[
                'path'    =>['label'=>'Image'],
                'alt'    =>['label'=>'Description du pictogramme (SEO)'],

            ],
        ],
        'testimonials'             =>[
            'fields'=>[
                'visible'    =>['label'=>'Affiché ?'],
                'pic'    =>['label'=>'Image'],
                'caption'    =>['label'=>'Nom / Fonction'],
                'text'    =>['label'=>'Témoignage'],

            ],
        ],

    ],
    'job_types' => [
        'list'=>[
            'name'    =>['label'=>'Type de client'],
        ],
        'fields'=>[
            'name'    =>['label'=>'Type de client'],

        ],

    ],
];