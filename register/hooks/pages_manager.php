<?php
use Skimia\Backend\Managers\Bridge;

Hook::register('activities.pages_manager.sidenav-item',function(Bridge $bridge){



    $bridge->items = [
        'portfolio'=>[
            'icon'        => 'os-icon-tasks',
            'name'        => 'Références',
            'state'       => 'jobs_manager',
            'type'        => 'menu',
            'color'       => 'brown',
            'items'       => [
            [
                'icon'        => 'os-icon-tasks',
                'name'        => 'Références',
                'state'       => 'jobs_manager.jobs*list',
                'description' => 'Gestion de vos références clients',
            ],
            [
                'icon'        => 'os-icon-flow-cascade',
                'name'        => 'Types',
                'state'       => 'jobs_manager.jobs_t*list',
                'description' => 'Types de References',
            ]
        ],
    ]
    ];
},1500);