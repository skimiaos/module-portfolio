<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 05/03/2015
 * Time: 15:15
 */
namespace Skimia\Portfolio\Components;

use Illuminate\Support\Collection;
use Skimia\Pages\Components\Component;
use Skimia\Pictures\Data\Models\Slider\Slider as SliderEntity;
use Skimia\Portfolio\Data\Models\Job;

class Jobs extends Component{

    protected static $systemName = 'jobs';

    protected $name = 'Références';
    protected $description = 'afficher la liste de vos traveaux';
    protected $icon = 'os-icon-tasks';

    protected $show_template = 'skimia.portfolio::components.jobs';


    protected function makeFields(){

        $this->fields = [

        ];
        $this->fields['_identifier']= ['type'=>'text','label'=>'Identifier','required'];
        $this->fields = new Collection($this->fields);
        $this->fieldsMaked = true;
        return $this;
    }

    public function onShow($merge_config = array())
    {
        $merged = $this->position->getConfiguration();

        $merged['jobs'] = Job::all();
        $merge_config = array_merge ( $merge_config, $merged ) ;

        return $merge_config;
    }

    protected $fields  = [

    ];

    public function getStaticJS()
    {
        return '';
        return file_get_contents(module_assets('skimia.pictures','/owl.carousel/owl.carousel.min.js'));
    }

    public function getDynJS()
    {
        return '';
        //dd($this->onJavascript());
        return \View::make('skimia.pictures::components.slider-js',$this->onJavascript())->render();
    }

    public function getStaticCSS()
    {
        return '';
        return file_get_contents(module_assets('skimia.pictures','/owl.carousel/assets/owl.carousel.css'))."\n".
        file_get_contents(module_assets('skimia.pictures','/owl.carousel/assets/owl.theme.default.css'));
    }

    public function getDynCSS()
    {
        return '';
    }
}