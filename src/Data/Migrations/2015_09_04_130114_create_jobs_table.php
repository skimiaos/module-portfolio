<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJobsTable extends Migration {

	public function up()
	{
		Schema::create('jobs', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('type');
			$table->text('picture');
			$table->string('client', 255);
			$table->text('caption');
			$table->text('testimonials');
		});
	}

	public function down()
	{
		Schema::drop('jobs');
	}
}