<?php

namespace Skimia\Portfolio\Data\Forms;

use Eloquent;
use Skimia\Angular\Form\CRUD\ActionOptionsInterface;
use Skimia\Angular\Form\CRUD\Actions\Create\CreateCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Delete\DeleteRestActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Edit\EditCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Get\GetCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudColumnConfiguration;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudFilterConfiguration;
use Skimia\Backend\Data\Models\Dashboard;
use Skimia\Auth\Traits\Acl;

use Skimia\Angular\Form\CRUD\CRUDForm;
use Skimia\Angular\Form\CRUD\Options;
use Orchestra\Model\Role;
use Skimia\Angular\Form\CRUD\OptionsInterface;
use Skimia\Menus\Data\Models\Item\LinkMenuItem;
use Skimia\Menus\Data\Models\Menu;
use Skimia\News\Data\Models\Post;
use Skimia\Portfolio\Data\Models\Job;
use Skimia\Portfolio\Data\Models\JobType;

class JobsCRUDForm extends CRUDForm{

    use Acl;
    use ListCrudActionTrait;
    use EditCrudActionTrait;
    use DeleteRestActionTrait;
    use CreateCrudActionTrait;

    /**
     * @return Eloquent
     */
    protected function getNewEntity()
    {
        return new Job();
    }

    protected function configure(OptionsInterface $options)
    {
        //First if you want use automatic Translation, set translation context
        $options->setTranslationContext('skimia.portfolio::form.jobs');
        //Second set global options
        $options->Access()->simpleAccess(false);


        $options->Fields()->makeSelectField('type')
            ->setProgrammingChoices(function(){
                return JobType::lists('name','id');
            })
            ->transAll()
            ->setDisplayOrder(1000);

        $pic = $options->Fields()->makeAssociativeField('picture')
            ->transAll()
            ->setDisplayOrder(1000);
        $pic->Fields()->makeImageField('path')->transAll();
        $pic->Fields()->makeTextField('alt')->transAll();

        $options->Fields()->makeTextField('client')->transAll();
        $options->Fields()->makeTextareaField('caption')->transAll();


        $pic = $options->Fields()->makeAssociativeField('testimonials')
            ->transAll()
            ->setDirection('column')
            ->setDisplayOrder(1000);
        $pic->Fields()->makeCheckboxField('visible')->transAll();
        $pic->Fields()->makeImageField('pic')->transAll()->setNgShow('form.testimonials.visible');
        $pic->Fields()->makeTextField('caption')->transAll()->setNgShow('form.testimonials.visible');
        $pic->Fields()->makeWYSIWYGField('text')->transAll()->setNgShow('form.testimonials.visible');


    }

    protected function configureActions(ActionOptionsInterface $options)
    {
        //TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$LIST_REST_ACTION)->setIcon('os-icon-tasks')->setTitle('Liste de vos Références');//TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$CREATE_REST_ACTION)->setIcon('os-icon-tasks')->setTitle('Créer une nouvelle référence');//TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$EDIT_REST_ACTION)->setIcon('os-icon-tasks')->setTitle('Editer votre Référence');//TRANSFO RELATIONNELLES


        //$options->ActionFields(self::$LIST_REST_ACTION)->makeRelationField('roles')->ManyToManyRelation()->displayColumns(['name']);


        $this->listConfiguration->addIdColumn();
        $this->listConfiguration->getNewColumnDefinition('type')->type(ListCrudColumnConfiguration::_TYPE_SELECT)->automaticTranslatedDisplayName();
        $this->listConfiguration->getNewColumnDefinition('picture')->type(ListCrudColumnConfiguration::_TYPE_PICTURE)->setSelector('picture.path')->automaticTranslatedDisplayName();
        $this->listConfiguration->getNewColumnDefinition('client')->type(ListCrudColumnConfiguration::TYPE_STRING)->automaticTranslatedDisplayName();









    }

    /**
     * @return string
     */
    public function getCRUDName()
    {
        return 'jobs';
    }

}