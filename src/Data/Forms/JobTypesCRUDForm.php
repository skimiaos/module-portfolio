<?php

namespace Skimia\Portfolio\Data\Forms;

use Eloquent;
use Skimia\Angular\Form\CRUD\ActionOptionsInterface;
use Skimia\Angular\Form\CRUD\Actions\Create\CreateCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Delete\DeleteRestActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Edit\EditCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Get\GetCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudActionTrait;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudColumnConfiguration;
use Skimia\Angular\Form\CRUD\Actions\Lists\ListCrudFilterConfiguration;
use Skimia\Backend\Data\Models\Dashboard;
use Skimia\Auth\Traits\Acl;

use Skimia\Angular\Form\CRUD\CRUDForm;
use Skimia\Angular\Form\CRUD\Options;
use Orchestra\Model\Role;
use Skimia\Angular\Form\CRUD\OptionsInterface;
use Skimia\Menus\Data\Models\Item\LinkMenuItem;
use Skimia\Menus\Data\Models\Menu;
use Skimia\News\Data\Models\Post;
use Skimia\Portfolio\Data\Models\Job;
use Skimia\Portfolio\Data\Models\JobType;

class JobTypesCRUDForm extends CRUDForm{

    use Acl;
    use ListCrudActionTrait;
    use EditCrudActionTrait;
    use DeleteRestActionTrait;
    use CreateCrudActionTrait;

    /**
     * @return Eloquent
     */
    protected function getNewEntity()
    {
        return new JobType();
    }

    protected function configure(OptionsInterface $options)
    {
        //First if you want use automatic Translation, set translation context
        $options->setTranslationContext('skimia.portfolio::form.job_types');
        //Second set global options
        $options->Access()->simpleAccess(false);



        $options->Fields()->makeTextField('name')->transAll();


    }

    protected function configureActions(ActionOptionsInterface $options)
    {
        //TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$LIST_REST_ACTION)->setIcon('os-icon-tasks')->setTitle('Liste des Types de références');//TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$CREATE_REST_ACTION)->setIcon('os-icon-tasks')->setTitle('Ajouter un nouveau type');//TRANSFO RELATIONNELLES
        $options->ActionTemplate(self::$EDIT_REST_ACTION)->setIcon('os-icon-tasks')->setTitle('Renommer le type');//TRANSFO RELATIONNELLES


        //$options->ActionFields(self::$LIST_REST_ACTION)->makeRelationField('roles')->ManyToManyRelation()->displayColumns(['name']);


        $this->listConfiguration->addIdColumn();
        $this->listConfiguration->getNewColumnDefinition('name')->type(ListCrudColumnConfiguration::TYPE_STRING)->automaticTranslatedDisplayName();









    }

    /**
     * @return string
     */
    public function getCRUDName()
    {
        return 'jobs_t';
    }

}