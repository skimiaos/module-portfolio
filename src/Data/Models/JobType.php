<?php

namespace Skimia\Portfolio\Data\Models;

use Eloquent;


class JobType extends Eloquent{


	protected $table = 'job_types';
	public $timestamps = true;


}