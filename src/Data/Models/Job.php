<?php

namespace Skimia\Portfolio\Data\Models;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Eloquent;
use League\Flysystem\Exception;

class Job extends Eloquent{



	protected $table = 'jobs';
	public $timestamps = true;

	public function getTestimonialsAttribute($value){
		if(!isset($value) ||empty($value))
			return [];
		return unserialize($value);
	}

	public function setTestimonialsAttribute($options){
		$this->attributes['testimonials'] = serialize($options);
	}

	public function getPictureAttribute($value){
		if(!isset($value) ||empty($value))
			return [];
		return unserialize($value);
	}

	public function setPictureAttribute($options){
		$this->attributes['picture'] = serialize($options);
	}

}